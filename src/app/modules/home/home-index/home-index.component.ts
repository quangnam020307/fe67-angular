import { HttpClient } from '@angular/common/http';
import {
  Component,
  OnInit,
  AfterViewInit,
  OnChanges,
  OnDestroy,
  Input,
  SimpleChanges,
} from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { IMovie } from 'src/app/models/movie';
import { MovieDataService } from 'src/app/services/movie-data.service';

@Component({
  selector: 'app-home-index',
  templateUrl: './home-index.component.html',
  styleUrls: ['./home-index.component.scss'],
})
export class HomeIndexComponent
  implements OnInit, AfterViewInit, OnChanges, OnDestroy
{
  @Input() data: any;
  //dependency injection
  constructor(
    private _movieService: MovieDataService,
    private _http: HttpClient
  ) {}

  moveList: IMovie[] = [];

  movieListSubcription: Subscription | undefined;

  fetchMovies(): void {
    this._http
      .get('https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim', {
        params: {
          maNhom: 'GP01',
        },
        headers: {
          TokenCybersoft:
            'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJGcm9udCBFbmQgNjciLCJIZXRIYW5TdHJpbmciOiIyOS8wMS8yMDIyIiwiSGV0SGFuVGltZSI6IjE2NDM0MTQ0MDAwMDAiLCJuYmYiOjE2MTc1NTU2MDAsImV4cCI6MTY0MzU2MjAwMH0.N1IDGkovxIU1E2CjtI_QtEJksOO3lxZxuIwXABaa45w',
        },
      })
      .subscribe(
        (res: any) => {
          //cập nhật dữ liệu trên service
          this._movieService.setMovieList(res.content);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  handleDelete(): void {
    this._movieService.deleteMovie(1288);
  }

  // lifecycle chạy lần đầu tiên, tương tự với didmount()
  ngOnInit(): void {
    //call api fetch movie List
    this.fetchMovies();

    this.movieListSubcription = this._movieService.movieList.subscribe(
      (val) => {
        this.moveList = val;
      }
    );
  }

  // lifecycle chạy sau khi giao diện render xong
  ngAfterViewInit() {
    console.log('view Init');
  }
  // lifecycle chayj khi component nhận vào input từ cha và input bị thay đổi
  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
  }
  // lifecycle chạy khi component bị huỷ (tương tự componentWillUnmount của react)
  ngOnDestroy() {
    // ? : optional chainging
    this.movieListSubcription?.unsubscribe();
  }
}

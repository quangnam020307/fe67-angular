import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeIndexComponent } from '../home/home-index/home-index.component';
import { DetailIndexComponent } from '../detail/detail-index/detail-index.component';
import { SingInComponent } from '../auth/sing-in/sing-in.component';
import { SignUpComponent } from '../auth/sign-up/sign-up.component';
import { MovieNewComponent } from '../movies/movie-new/movie-new.component';

const routes: Routes = [
  { path: 'detail/:id', component: DetailIndexComponent },
  { path: 'signin', component: SingInComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'movie/new', component: MovieNewComponent },
  { path: '', component: HomeIndexComponent },
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forRoot(routes)],
})
export class RoutingModule {}
